package io.robusta.fora.business;

import io.robusta.fora.dao.ITopicDao;
import io.robusta.fora.domain.ITopic;

import java.util.List;
import java.util.logging.Logger;


public class TopicBusiness {

	private final  static Logger logger = Logger.getLogger(TopicBusiness.class.getName());

	ITopicDao dao;


	public ITopic getTopicById(long id) {
		return dao.getTopicById(id);
	}
	
	
	
	public List<ITopic> getAllTopics() {
		return dao.getAllTopics();		
	}	
	
	
	public int countTopics(){
		return dao.countTopics();	
	}
	
	
	public void setDao(ITopicDao dao) {
		this.dao = dao;
	}
}
