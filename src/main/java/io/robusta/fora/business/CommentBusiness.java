package io.robusta.fora.business;

import io.robusta.fora.dao.ICommentDao;
import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.ITopic;
import io.robusta.fora.domain.IUser;
import io.robusta.fora.domain.impl.Comment;
import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CommentBusiness {

	ICommentDao dao;

    @Autowired
    Log log;


	public IComment getCommentById(String id) {

        log.info("Logging right now");
		return dao.getCommentById(id);

	}

	public IComment createComment(ITopic topic, String content, IUser u,
			boolean anonymous) {
		
		Comment comment = new Comment();
		comment.setAnonymous(anonymous);
		if (!anonymous) {
			comment.setUser(u);
		}
		comment.setContent(content);
		
		return dao.createComment(topic, comment);
		
	
	}
	
	public void setDao(ICommentDao dao) {
		this.dao = dao;
	}
}
