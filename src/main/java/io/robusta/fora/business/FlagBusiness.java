package io.robusta.fora.business;

import io.robusta.fora.dao.IFlagDao;
import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.impl.Flag;

import java.util.logging.Logger;

public class FlagBusiness {


	CommentBusiness commentBusiness;
	IFlagDao dao;

	private final  static Logger logger = Logger.getLogger(FlagBusiness.class.getName()); 
	
	public synchronized void flagComment(String commentId, String flagMessage){
		
		logger.info("flagging comment "+commentId+" with "+flagMessage);
		
		IComment comment = commentBusiness.getCommentById(commentId);
		Flag flag = new Flag();

		dao.flagComment(comment, flag);
		
	}

	public void setCommentBusiness(CommentBusiness commentBusiness) {
		this.commentBusiness = commentBusiness;
	}

	public void setDao(IFlagDao dao) {
		this.dao = dao;
	}
	
	
	
}
