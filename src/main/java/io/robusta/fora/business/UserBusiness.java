package io.robusta.fora.business;

import io.robusta.fora.dao.IUserDao;
import io.robusta.fora.domain.IUser;
import io.robusta.fora.domain.impl.User;

import java.util.logging.Logger;

public class UserBusiness {

	private final static Logger logger = Logger.getLogger(UserBusiness.class
			.getName());

	IUserDao dao;

	public IUser getOrCreateUser(String email) {
		IUser u = getUserByEmail(email);
		if (u != null){
			return u;
		}else{
			return createUser(email);
		}
		
		
	}
	
	 public IUser getUserByEmail(String email){
		 return dao.getUserByEmail(email);
	 }


    public IUser getUserById(long id){

    	return dao.getUserById(id);
    }
	
	public  IUser createUser(String email){
		User u = new User();
		u.setEmail(email);

		IUser result = dao.createUser(u);
		logger.info("Created user "+email);
		return result;
	}

    public void updateUser(IUser user){
    	//invalid caches
    	dao.updateUser(user);
    }


    public void deleteUser(IUser user){
    	//invalid caches
        dao.deleteUser(user);
    }

    public void setDao(IUserDao dao) {
		this.dao = dao;
	}
}
