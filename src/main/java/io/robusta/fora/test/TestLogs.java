package io.robusta.fora.test;

import com.google.gson.Gson;
import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.dao.local.ForaDataSource;
import io.robusta.fora.domain.IForum;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by  Nicolas Zozol for Robusta Code
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class TestLogs {

    public static void main(String[] args) {
        checkLogs();
    }

    public static void checkLogs(){
        System.setProperty("org.apache.commons.logging.simplelog.showlogname", "true");
        AbstractApplicationContext context = new ClassPathXmlApplicationContext("**/Forum.xml");
        IForum monForum = (IForum) context.getBean("monForum");

        Gson gson = new Gson();
        System.out.println(gson.toJson(monForum));

        CommentBusiness commentBusiness = (CommentBusiness) context.getBean("commentBusiness");
        System.out.println(commentBusiness.getCommentById(ForaDataSource.NOT_OK_COMMENT_ID));

        context.close();


    }
}
