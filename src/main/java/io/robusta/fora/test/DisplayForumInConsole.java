package io.robusta.fora.test;

import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.dao.local.ForaDataSource;
import io.robusta.fora.domain.IForum;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.google.gson.Gson;

public class DisplayForumInConsole {
	public static void main(String[] args) {
		DisplayInConsole();
	}
	
	public static void DisplayInConsole(){
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("**/Forum.xml");
		IForum monForum = (IForum) context.getBean("monForum");
		
		Gson gson = new Gson();
		System.out.println(gson.toJson(monForum));
		
		CommentBusiness commentBusiness = (CommentBusiness) context.getBean("commentBusiness");
		System.out.println(commentBusiness.getCommentById(ForaDataSource.NOT_OK_COMMENT_ID));
		
		context.close();
		
		
	}
}
