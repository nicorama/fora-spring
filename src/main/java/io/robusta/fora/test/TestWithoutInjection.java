package io.robusta.fora.test;

import io.robusta.fora.domain.IForum;
import io.robusta.fora.domain.impl.Forum;


public class TestWithoutInjection {

	public static void main(String[] args) {
		IForum forum = new Forum();
		
		forum.setName("Mon forum");
		forum.setDescription("Un forum remplit de donnees un peu aleatoires");
		forum.setAdmin(TestFactory.getAdmin());
		forum.addTopic(TestFactory.getPopulatedTopic() );

		System.out.print(forum);
	}

}
