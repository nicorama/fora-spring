package io.robusta.fora.test;
import io.robusta.fora.domain.IAdmin;
import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.ITopic;
import io.robusta.fora.domain.IUser;
import io.robusta.fora.domain.impl.Admin;
import io.robusta.fora.domain.impl.Comment;
import io.robusta.fora.domain.impl.Topic;
import io.robusta.fora.domain.impl.User;

public class TestFactory {
	protected static int nbUser = 0;
	protected static int nbMessage = 0;
	protected static int nbTopic = 0;

	protected static String[] users = {"Simon", "Nico", "Albert", "Siska", "Magali", "..."};
	protected static String[] topics= {"Contenu des formation", "Date des formation"
			, "partenariat", "De l'usage des Big-Mac en astronavigation", "papotage"};
	protected static String[] messages={"L'economie des Pulau Baniak est principalement basee sur la peche. \n Environs 60% des hommes adultes de l'archipel sont pecheur. \n La seconde industrie exportatrice est la copra."
   			,"S'il y a des nuages dans le ciel \n C'est dieu qui se fait des pop-corn \n Il les fait frire en plein Soleil \n Copyright Dionisos"
   			,"Ce matin, je me suis leve a 11h. \n Je me suis fait un cafe et des nouilles \n Puis j'ai redige cette formation. \n J'ai ensuite pris mon scooter a propulseur Buzard pour aller au MacDo \n Parce que j'en ai marre de la bouffe de Sirius"
   			,"Le chat domestique (felix domus) est un carnivore obligatoire. \n En effet, comme les autres felin, il est incapable de synthetiser la Taurine. \n Cet acide amine est synthetise naturellement par tous les autres animaux. \n Les felins sont donc contraint de se la procurer par la consomation de viande animale. \n Ou a la limite, 5 Red-Bull par jour."
   			,"C'est l'histoire d'un programmeur Java qui repeint son plafond. \n Un commercial arrive et lui dit \n \"accroche-toi . J'ai reduit les delais de 15 jours\" "
			,"Masukan kartu itu ke dalam ponsel \n Aftifkan ponsel dan menu registrasi ke 4444 akan muncul \n Ikuti petunjuk registrasi \n Atau kirim SMS, ketik Daftar ke 4444." 
			,"2 brosse a dents \n 5 grands sachets de chips natures \n 2 lecteurs de disquettes (5.25\") \n Un fouet electrique \n 6 oeufs \n 2 silencieux" };
		
	public static IUser  getUser(){
		nbUser++;
		return  new User(nbUser, users[nbUser % users.length]
				       , users[nbUser % users.length]+"@robusta.io");
	}
	public static IAdmin  getAdmin(){
		nbUser++;
		return  new Admin(nbUser, users[nbUser % users.length]
				       , users[nbUser % users.length]+"@robusta.io", "hey !");
	}
	
	public static IComment getComment (){
		IComment comment = new Comment();
		comment.setUser(getUser());
		comment.setContent(messages[nbMessage % messages.length]);
		nbMessage++;
		return comment;
	}
	
	public static ITopic getTopic( ){  //TODO admin optionel
		ITopic topic = new Topic();
		
		topic.setUser(getUser());
		topic.setTitle(topics[nbTopic % topics.length]);
		
		
		nbTopic ++;
		return topic;
	}

	/**
	 * Reprend getTopic et ajoute 5 messages
	 * @return un objet ITopic peuple avec un admin et 5 messages dedans
	 */
	public static ITopic getPopulatedTopic() {
		ITopic topic = getTopic();
		
		for (int i=0; i<5; i++){
			topic.addComment(getComment());
		}
		
		return topic;
	}
	
	
}
