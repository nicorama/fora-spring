package io.robusta.fora.servlets;

import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.domain.IComment;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

/**
 * Created by Nicolas
 * Date: 13/02/14
 * Time: 11:06
 */
@WebServlet("/comment/*")
public class CommentServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String uri = req.getRequestURI();
        String[] splits = uri.split("/");
        String id = splits[splits.length - 1];
        System.out.println("found id for comment : "+id);

        IComment c = new CommentBusiness().getCommentById(id);
        String json = new Gson().toJson(c);
        resp.setContentType("application/json");
        resp.getOutputStream().print(json);

    }



}
