package io.robusta.fora.servlets;

import io.robusta.fora.business.CommentBusiness;
import io.robusta.fora.domain.IComment;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;

@WebServlet("/comments/like")
public class LikeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {


		
		JsonReader reader = new JsonReader(new InputStreamReader(request.getInputStream()));
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject)parser.parse(reader); 
		
		
		String id = obj.get("id").getAsString();
		IComment c = new CommentBusiness().getCommentById(id);
		
		boolean like = obj.get("like").getAsBoolean();
		
		if (like){
			c.up();
		}else{
			c.down();
		}
			
		Result result = new Result(id, c.getScore());
		response.setContentType("application/json");
		response.getOutputStream().print(new Gson().toJson(result));

	}

}

class Result {
	String id;
	int like;

	public Result(String id, int like) {
		this.id = id;
		this.like = like;
	}
}
