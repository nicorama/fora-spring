package io.robusta.fora.servlets;

import io.robusta.fora.dao.local.ForaDataSource;
import io.robusta.fora.domain.IFlag;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/flag")
public class FlagServlet extends HttpServlet{

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		List<IFlag> flags = ForaDataSource.getInstance().getFlags();
		resp.getOutputStream().print(new Gson().toJson(flags));
		
	}
	
}
