package io.robusta.fora.servlets;

import io.robusta.fora.business.TopicBusiness;
import io.robusta.fora.domain.ITopic;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

@WebServlet("/topic/*")
public class TopicServlet extends HttpServlet{

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
			
			String uri = req.getRequestURI();
			String[] splits = uri.split("/");
			int id = Integer.valueOf(splits[splits.length - 1]);
			System.out.println(id);
			ITopic s = new TopicBusiness().getTopicById(id);
			String json = new Gson().toJson(s);
			resp.setContentType("application/json");
			resp.getOutputStream().print(json);		
	}
	
}
