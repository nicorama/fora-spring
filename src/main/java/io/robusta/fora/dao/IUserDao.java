package io.robusta.fora.dao;

import io.robusta.fora.domain.IUser;

public interface IUserDao {


	public IUser getUserById(long id);
	
	public IUser getUserByEmail(String email);

	public IUser createUser(IUser u);

	public void updateUser(IUser user);

	public void deleteUser(IUser user);
}
