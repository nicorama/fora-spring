package io.robusta.fora.dao;

import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.ITopic;

public interface ICommentDao {

	public IComment getCommentById(String id);
	
	IComment createComment(ITopic t, IComment c);
}
