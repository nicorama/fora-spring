package io.robusta.fora.dao;

import io.robusta.fora.domain.ITopic;

import java.util.List;

public interface ITopicDao {

	public ITopic getTopicById(long id);

	public List<ITopic> getAllTopics();

	public int countTopics();
}
