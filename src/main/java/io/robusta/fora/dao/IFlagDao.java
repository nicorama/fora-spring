package io.robusta.fora.dao;

import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.IFlag;

public interface IFlagDao {

	public void flagComment(IComment comment, IFlag flag);
}
