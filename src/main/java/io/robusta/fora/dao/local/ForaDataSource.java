package io.robusta.fora.dao.local;


import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.IFlag;
import io.robusta.fora.domain.ITopic;
import io.robusta.fora.domain.IUser;
import io.robusta.fora.domain.impl.Admin;
import io.robusta.fora.domain.impl.Comment;
import io.robusta.fora.domain.impl.Flag;
import io.robusta.fora.domain.impl.Topic;
import io.robusta.fora.domain.impl.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class ForaDataSource {
	
	public static final String NOT_OK_COMMENT_ID = "1";
	public static final long TROLL_TOPIC_ID = 1L;
	public static final String You_dont_know_enough_COMMENT_ID = "2";
	protected static ForaDataSource instance;
	
	List<IUser> users = new ArrayList<IUser>();
	List<ITopic> topics = new ArrayList<ITopic>();
	List<IComment> comments = new ArrayList<IComment>();
	List<IFlag> flags = new ArrayList<IFlag>();
	ITopic troll;
	
	private ForaDataSource(){
		
		initUsers();
		initTopics();

		//fillMany(24);
		
	}
	
	protected ForaDataSource (boolean create){
		ForaDataSource instance = getInstance();
		if (create){
			this.topics = instance.topics;
			this.comments = instance.comments;
			this.users = instance.users;
			this.flags = instance.flags;
			this.troll = instance.troll;
		}
		
		
		
	}
	
	public static ForaDataSource getInstance(){
		if (instance == null){
			instance = new ForaDataSource();			
		}
		return instance;
	}

	
	private void initUsers(){
		User nicolas = new Admin("Star Wars rocks !");
		nicolas.setId(1L);
		nicolas.setEmail("nz@robusta.io");
		nicolas.setName("Nicolas");

		User leonard = new Admin("Star Trek rocks");
        leonard.setId(2L);
        leonard.setName("Leonard");
        leonard.setEmail("leonard@robusta.io");

        User  sheldon = new User();
        sheldon.setId(3L);
        sheldon.setName("Sheldon");
        sheldon.setEmail("sheldon@robusta.io");

        User  raj = new User();
        raj.setId(4L);
        raj.setName("Raj");
        raj.setEmail("raj@robusta.io");

        User  howard = new User();
        howard.setId(5L);
        howard.setName("Howard");
        howard.setEmail("howard@robusta.io");

        User  penny = new User();
        penny.setId(6L);
        penny.setName("Penny");
        penny.setEmail("penny@robusta.io");

        User emy = new User();
        emy.setId(7L);
        emy.setName("Emy");
        emy.setEmail("emy@robusta.io");

        User bernie = new User();
        bernie.setId(8L);
        bernie.setName("Bernadette");
        bernie.setEmail("bernie@robusta.io");

		
		Collections.addAll(this.users, nicolas, leonard, sheldon, raj, howard,   penny, emy,bernie);
	}
	
	
	private void initTopics(){
		troll = new Topic();
		troll.setId(TROLL_TOPIC_ID);
		troll.setTitle("Star Trek > Star Wars");
		troll.setContent("*Spock* is stronger than Yoda");
        troll.setUser(leonard());

		Topic kids = new Topic();
		kids.setId(2L);
		kids.setTitle("Kids ar' cool");
		kids.setContent("But I'm so <strong>tired</strong> <script type='text/javascript'>alert('you're fired!')</script>");

		// Usually, we would like to make business stuff with a addComment
		// method
		troll.setComments(this.initTrollComments());
        Collections.addAll(this.topics, troll, kids);
	}

	public List<IUser> getUsers(){		
		return this.users;
	}
	
	public IUser nicolas(){
		return this.getUsers().get(0);
	}
	
	public IUser leonard(){
		return this.getUsers().get(1);
	}

    public IUser sheldon(){
        return this.getUsers().get(2);
    }
	
	public List<ITopic> getTopics() {
		return this.topics;
	}
	
	public List<IComment> getComments() {
		
		return this.comments;
	}
	
	
	
	private List<IComment> initTrollComments(){
		
		Comment c1 = new Comment();
		c1.setId(NOT_OK_COMMENT_ID);
		c1.setAnonymous(true);
		c1.setContent("I'm not ok");

		Comment c2 = new Comment();
		c2.setId(You_dont_know_enough_COMMENT_ID);
		c2.setUser(leonard());
		c2.setContent("You don't know enough about heroes");

		Comment c3 = new Comment();
		c3.setId("3");
		c3.setAnonymous(true);
		c3.setContent("What ? You stupid !");
		
		Flag flag = new Flag();
		flag.setId(1L);
		flag.setContent("This guy went too far.");
		c3.getFlags().add(flag);
		flags.add(flag);
		
		List<IComment>result = new ArrayList<IComment>();		
		Collections.addAll(result, c1, c2, c3);
		this.comments.addAll(result);
		return result; 
	}
	
	public List<IComment> getTrollComments() {
		return troll.getComments();
	}

	public List<IFlag> getFlags() {
		return flags;
	}


	
	public int getTotalCommentsCount(){
		int count = 0;
		for (ITopic s : this.getTopics()){
			count+=s.getComments().size();
		}
		return count;
	}
	
	
	public void fillMany(int size) {
		// int size = 24;
		int userSize = size;
		int topicSize = userSize * 3;
		int commentSize = userSize * 12;
		int flagSize = userSize;

		for (int i = 0; i < userSize; i++) {
			User u = new User();
			u.setEmail("user" + i + "@fora.com");
			u.setName("John Doe - " + i);
			users.add(u);
		}

		for (int i = 0; i < topicSize; i++) {
			Topic s = new Topic();
			s.setContent("Some content " + i);
			s.setTitle("Title " + i);
			IUser u = getRandomItem(IUser.class, users);			
			s.setUser(u);
			topics.add(s);
		}

		for (int i = 0; i < commentSize; i++) {
			Comment c = new Comment();
			c.setUser(getRandomItem(IUser.class, users));
			c.setContent("My comment says " + i);
			ITopic s = getRandomItem(ITopic.class, topics);
			s.getComments().add(c);
			comments.add(c);
		}

		for (int i = 0; i < flagSize; i++) {
			Flag flag = new Flag();
			IComment c = getRandomItem(IComment.class, comments);
			flag.setContent("This is a flag for `" + c.getContent()+"`");
			c.getFlags().add(flag);
			flags.add(flag);
		}

	}

	public <T> T getRandomItem(Class<T> clazz, List<T> list) {

		int length = list.size();

		try {
			int index = new Random().nextInt(length);
			return list.get(index);
		} catch (RuntimeException e) {
			System.out.println("length is " + length);
			throw e;
		}

	}


}
