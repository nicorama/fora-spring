package io.robusta.fora.dao.local;

import io.robusta.fora.dao.IUserDao;
import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.ITopic;
import io.robusta.fora.domain.IUser;

public class LocalUserDao  implements IUserDao{
	
	ForaDataSource fora = ForaDataSource.getInstance();

	

	public IUser getUserByEmail(String email){
		for (IUser u : fora.getUsers()){
			if (u!=null && u.getEmail() != null && u.getEmail().equalsIgnoreCase(email)){
				return u;
			}
		}
		return null;
	}

    public IUser getUserById(long id){
        for (IUser u : fora.getUsers()){
            if (u!=null && u.getId() == id){
                return u;
            }
        }
        return null;
    }
	
	public  IUser createUser(IUser u){	
		synchronized (fora) {
			u.setId((long) fora.getUsers().size());
			fora.getUsers().add(u);
		}
		
		
		return u;
	}

    public void updateUser(IUser user){
        synchronized (fora) {
            for (int i = 0 ; i < fora.getUsers().size() ; i++){
                IUser u = fora.getUsers().get(i);

                if (u!=null && u.getId() == user.getId()){
                    //replacing
                    fora.getUsers().remove(i);
                    fora.getUsers().add(i, user);
                }
            }
        }
    }


    public void deleteUser(IUser user){
        ForaDataSource.getInstance().getUsers().remove(user);
        for (ITopic topic : ForaDataSource.getInstance().getTopics()){
            if (topic.getUser().equals(user)){
                topic.setUser(null);
            }
        }

        for (IComment comment: ForaDataSource.getInstance().getComments()){
            if (comment.getUser().equals(user)){
                comment.setUser(null);
                comment.setAnonymous(true);
            }
        }
    }

}
