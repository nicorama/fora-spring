package io.robusta.fora.dao.local;

import io.robusta.fora.dao.ITopicDao;
import io.robusta.fora.domain.ITopic;

import java.util.List;

public class TopicDao implements ITopicDao {

	
	ForaDataSource fora = ForaDataSource.getInstance();

	public ITopic getTopicById(long id) {
		synchronized (fora) {
			for (ITopic s : fora.getTopics()) {
				if (s.getId() == id) {
					return s;
				}
			}
		}

		return null;// or throw exception
	}

	public List<ITopic> getAllTopics() {
		return fora.getTopics();

	}

	public int countTopics() {
		synchronized (fora) {
			return fora.getTopics().size();
		}

	}
}
