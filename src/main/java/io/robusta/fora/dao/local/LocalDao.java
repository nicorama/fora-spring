package io.robusta.fora.dao.local;

import io.robusta.fora.dao.ICommentDao;
import io.robusta.fora.dao.IFlagDao;
import io.robusta.fora.domain.IComment;
import io.robusta.fora.domain.IFlag;
import io.robusta.fora.domain.ITopic;

public class LocalDao implements ICommentDao, IFlagDao{

	ForaDataSource fora = ForaDataSource.getInstance();
	
	
	@Override
	public IComment getCommentById(String id) {
		synchronized (fora) {
			for (IComment c : fora.getComments()) {
				if (c.getId().equals(id)) {
					return c;
				}
			}
		}
		return null;// or throw exception ?
	}

	@Override
	public IComment createComment(ITopic t, IComment c) {
		
		t.getComments().add(c);
		synchronized (fora) {
			fora.getComments().add(c);
		}
		return c;
	}

	@Override
	public synchronized void flagComment(IComment comment, IFlag flag){
				
		comment.getFlags().add(flag);
		synchronized(fora){
			fora.getFlags().add(flag);
		}
		
	}

}
