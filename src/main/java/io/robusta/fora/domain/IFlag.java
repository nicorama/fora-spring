package io.robusta.fora.domain;

public interface IFlag {

	public Long getId();

	public void setId(Long id);

	public String getContent();

	public void setContent(String content);
	
	
}
