package io.robusta.fora.domain;

public interface IUser {

	public Long getId();

	public void setId(Long id);

	public String getEmail();

	public void setEmail(String email);

	public String getName() ;

	public void setName(String name);

	  public boolean isAdmin();
}
