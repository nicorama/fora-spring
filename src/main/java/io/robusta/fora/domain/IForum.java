package io.robusta.fora.domain;

import java.util.List;

public interface IForum {

	List<ITopic> getTopics();
	void setTopics(List <ITopic> topics);
	
	List<IUser> getUsers();
	void setUsers(List<IUser> users);
	
	String getName();
	String getDescription();
	
	void setName(String name);
	void setDescription(String description);
	
	IAdmin getAdmin();
	void setAdmin(IAdmin admin);
	
	
	void addTopic(ITopic topic);
	
	
	
}

