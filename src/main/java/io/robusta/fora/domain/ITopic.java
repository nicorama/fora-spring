package io.robusta.fora.domain;

import io.robusta.fora.domain.impl.Comment;

import java.util.List;

public interface ITopic extends Flagable{

	public long getId();

	public void setId(long id);

	public String getTitle() ;

	public void setTitle(String title);

	public String getContent();

	public void setContent(String content);

	public List<IComment> getComments();

	public void setComments(List<IComment> comments);

	@Override
	public boolean isFlagged() ;

	@Override
	public List<IFlag> getFlags() ;

	public void setFlags(List<IFlag> flags);

	public IUser getUser() ;

	public void setUser(IUser user) ;
	
	public void addComment(IComment comment);
}
