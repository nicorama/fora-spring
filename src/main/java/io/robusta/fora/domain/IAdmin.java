package io.robusta.fora.domain;

public interface IAdmin extends IUser {

	public String getStatement();
	public void setStatement(String statement);
}
