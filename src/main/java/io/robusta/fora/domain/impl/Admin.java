package io.robusta.fora.domain.impl;

import io.robusta.fora.domain.IAdmin;

/**
 * Created by Nicolas
 * Date: 15/02/14
 * Time: 21:26
 */
public class Admin extends User implements IAdmin {

    String statement;

    public Admin(String statement) {
        this.statement = statement;
        this.admin = true;
    }
    
    public Admin(long id, String email, String name, String statement) {
  		super(id, email, name);
  		this.statement = statement;
  	}

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }
}
