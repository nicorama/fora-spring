package io.robusta.fora.domain.impl;

import io.robusta.fora.domain.IUser;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class User implements Serializable, IUser{

	
	private static final long serialVersionUID = 3490373199478816786L;
	
	Long id;	
	String email;
	String name;
	String lastname;
	
	int version=1;
    protected boolean admin = false;
	
    public User() {
		// TODO Auto-generated constructor stub
	}
    
    
    public User(long id, String email, String name) {
		this.id = id;
		this.email = email;
		this.name = name;
	}
    
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	

	
	//Using id and email
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (email != null){
			return email;
		}else{
			return "Anonymous";
		}
		
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}


    public boolean isAdmin() {
        return admin;
    }
}
