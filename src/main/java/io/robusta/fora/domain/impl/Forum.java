package io.robusta.fora.domain.impl;

import io.robusta.fora.domain.IAdmin;
import io.robusta.fora.domain.IForum;
import io.robusta.fora.domain.ITopic;
import io.robusta.fora.domain.IUser;

import java.util.ArrayList;
import java.util.List;

public class Forum implements IForum{

	List<ITopic> topics = new ArrayList<ITopic>();
	IAdmin admin;
	List<IUser> users = new ArrayList<IUser>();
	
	String name;
	String description;
	
	
	public List<ITopic> getTopics() {
		return topics;
	}
	public void setTopics(List<ITopic> topics) {
		this.topics = topics;
	}
	public IAdmin getAdmin() {
		return admin;
	}
	public void setAdmin(IAdmin admin) {
		this.admin = admin;
	}
	public List<IUser> getUsers() {
		return users;
	}
	public void setUsers(List<IUser> users) {
		this.users = users;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Override
	public void addTopic(ITopic topic) {
		this.topics.add(topic);
		
	}
	
	@Override
	public String toString() {
		return "Forum "+this.name;
	}
	

	
	
}
