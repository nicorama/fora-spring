package io.robusta.fora.domain;

import java.util.List;

public interface IComment extends Flagable {

	public String getId();

	public void setId(String id);

	public String getContent();

	public void setContent(String content);

	public IUser getUser();

	public void setUser(IUser user);

	public boolean isAnonymous();

	public void setAnonymous(boolean anonymous);

	@Override
	public boolean isFlagged();

	@Override
	public List<IFlag> getFlags();

	public void setFlags(List<IFlag> flags);

	public void up();

	public void down();

	public int getScore();

}
